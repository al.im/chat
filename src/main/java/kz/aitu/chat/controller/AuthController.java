package kz.aitu.chat.controller;

import kz.aitu.chat.model.Authorization;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.UsersRepository;
import kz.aitu.chat.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/auth")
@AllArgsConstructor
public class AuthController {
    private final AuthService authService;
    private final UsersRepository usersRepository;


    @PostMapping
    public ResponseEntity<?> authentication(@RequestBody Authorization auth, @RequestParam(name = "username") String username) {
        Users user = new Users(username);
        return ResponseEntity.ok(user);
    }
}

