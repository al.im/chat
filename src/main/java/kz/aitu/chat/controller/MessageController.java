package kz.aitu.chat.controller;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.MessageRepository;
import kz.aitu.chat.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/messages")
@AllArgsConstructor
public class MessageController {
    private MessageService messageService;
    private MessageRepository messageRepository;


    @GetMapping("/chat/{chatId}")
    public ResponseEntity<?> getAll(@PathVariable Long chatId, @RequestParam(required = false) Long size){
        if(size == null) return ResponseEntity.ok(messageService.findAllByChatId(chatId));

        return ResponseEntity.ok(messageService.findAllByChatId(chatId, size));
    }



    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody Message message) throws Exception{
        return ResponseEntity.ok(messageService.addMessage(message));
    }

    @PutMapping("")
    public ResponseEntity<?> updateMessage(@RequestBody Message message) throws Exception {
        return ResponseEntity.ok(messageService.updateMessage(message));
    }

    @DeleteMapping
    public ResponseEntity<?> deleteMessage(@PathVariable Long id){
        messageService.deleteMessage(id);
        return ResponseEntity.noContent().build();
    }


}
