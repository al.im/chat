package kz.aitu.chat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "auth")
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Authorization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String login;
    private String password;

    @Column(name = "last_login_timestamp")
    private Long lastLoginTimestamp;

    @Column(name = "user_id")
    private Long userId;

    private String token;

}
