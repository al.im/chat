package kz.aitu.chat.service;

import kz.aitu.chat.repository.ParticipantRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ParticipantService {

    private ParticipantRepository participantRepository;

    public boolean isExistChatIdAndUserId(Long chatId, Long userId){
        return participantRepository.existsByChatIdAndUserId(chatId, userId);
    }


}
