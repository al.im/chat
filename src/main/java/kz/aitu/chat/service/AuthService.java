package kz.aitu.chat.service;


import kz.aitu.chat.model.Authorization;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.AuthRepository;
import kz.aitu.chat.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@AllArgsConstructor

public class AuthService {
    private final AuthRepository authRepository;
    private final UsersRepository usersRepository;

    public Authorization save(Authorization auth){
        return authRepository.save(auth);
    }

    public void authenticate(Authorization auth, Users user){
        Users newUser = UsersRepository.save(user);
        Long time = new Date().getTime();

        auth.setLastLoginTimestamp(time);
        auth.setUserId(newUser.getId());
        save(auth);

    }

}
