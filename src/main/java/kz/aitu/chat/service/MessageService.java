package kz.aitu.chat.service;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.MessageRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Data
public class MessageService {
    private MessageRepository messageRepository;
    private ParticipantService participantService;

    public List<Message> findAllByChatId(Long chatId){
        return messageRepository.findAllByChatId(chatId);
    }

    public List<Message> findAllByChatId(Long chatId,Long userId) {
        List<Message> messages = messageRepository.findAllByChatId(chatId);
        for (Message message : messages) {
            if (!message.getUser_id().equals(userId) && !message.isRead()) {
                message.setRead(true);
            }
        }
        return messageRepository.saveAll(messages);
    }




    public Message addMessage(Message message) throws Exception {

        if(message == null) throw new Exception("message is null");
        if(message.getId() != null) throw new Exception("message has id");
        if(message.getText() == null || message.getText().isBlank()) throw new Exception("message is blank");
        if(!participantService.isExistChatIdAndUserId(message.getChat_id(), message.getUser_id())) throw new Exception("forbidden");

        message.setCreatedTimestamp(new Date().getTime());
        message.setUpdatedTimestamp(new Date().getTime());

        return messageRepository.save(message);
    }



    public Message updateMessage(Message message) throws Exception{
        if(message.getId() == null) throw new Exception("message id is null");
        if(message.getText() == null || message.getText().isBlank()) throw new Exception("message is blank");

        Optional<Message> messageDB = messageRepository.findById(message.getId());
        if(messageDB.isEmpty()) throw new Exception("message not found");

        String text = message.getText();
        message = messageDB.get();

        message.setText(text);
        message.setUpdatedTimestamp(new Date().getTime());

        return messageRepository.save(message);
    }

    public Message save(Message message) {
        return messageRepository.save(message);
    }

    public void deleteMessage(Long messageId){
        messageRepository.deleteById(messageId);
    }


    public List<Message> findAllDeliveredChatId(Long chatId, Long userId){
        return messageRepository.findAllByDeliveredChatId(chatId, userId);
    }

}
