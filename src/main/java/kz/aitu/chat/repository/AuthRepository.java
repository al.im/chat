package kz.aitu.chat.repository;

import kz.aitu.chat.model.Authorization;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthRepository extends JpaRepository<Authorization,Long> {
    Authorization getByToken(String token);
    Authorization getByLoginAndPassword(String login, String password);
}

