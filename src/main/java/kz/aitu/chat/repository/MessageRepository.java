package kz.aitu.chat.repository;

import kz.aitu.chat.model.Message;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findAllByChatId(Long chatId);
    List<Message> findAllByChatId(Long chatId, SpringDataWebProperties.Pageable pageable);
    List<Message> findAllByDeliveredChatId(Long chatId, Long userId);


}
