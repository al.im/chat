alter table message
add column isRead boolean,
add column isDelivered boolean,
add column readTimestamp bigint,
add column deliveredTimestamp bigint;
